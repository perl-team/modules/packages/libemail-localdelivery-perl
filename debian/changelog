libemail-localdelivery-perl (1.201-2) unstable; urgency=medium

  * Remove generated test data via debian/clean. (Closes: #1046400)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Mar 2024 12:47:35 +0100

libemail-localdelivery-perl (1.201-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Import upstream version 1.201.
  * Update upstream email address.
  * Update years of packaging copyright.
  * Update debian/upstream/metadata.
  * Refresh 10mbox-locking.patch (offset).
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Update alternative test dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Remove unneeded test dependency: libcapture-tiny-perl.

 -- gregor herrmann <gregoa@debian.org>  Fri, 03 Jun 2022 20:13:45 +0200

libemail-localdelivery-perl (1.200-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 15:20:50 +0100

libemail-localdelivery-perl (1.200-1) unstable; urgency=low

  [ gregor herrmann ]
  * Change my email address.
  * debian/control: remove versions from (build) dependencies, all fulfilled
    in stable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 1.200
  * Refresh debian/copyright, converting to machine-readable format (1.0)
  * Drop obsolete README.source
  * Convert to source format 3.0 (quilt)
  * Convert to short-form debian/rules
  * Refresh debian/control using dh-make-perl
  * Drop libtest-pod-perl and libtest-pod-coverage-perl from
    build-dependencies, only needed for release testing
  * Refresh 10mbox-locking.patch (fuzz)
  * Update Homepage to metacpan, the old wiki has been nonexistent since 2009
  * Update short and long description, adding warning note

 -- Florian Schlichting <fsfs@debian.org>  Tue, 20 Aug 2013 21:19:51 +0200

libemail-localdelivery-perl (0.217-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.
  * debian/rules: delete /usr/lib/perl5 only if it exists.

  [ Damyan Ivanov ]
  * debian/watch: use dist-based URL, better pattern
  * debian/rules: sync with dh-make-perl templates
    + clean stamps before distclean
    + set CFLAGS according to $DEB_BUILD_OPTIONS
    + use $@ when touching stamps
    + drop unused dh_link
  * convert patches to quilt; fix target dependencies
  * Standards-Version: 3.7.3 (no changes)
  * debhelper compatibility level 6
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Fri, 18 Jan 2008 13:18:20 +0200

libemail-localdelivery-perl (0.217-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.
  * Adapt patch 10mbox-locking accordingly.

  [ Niko Tyni ]
  * Add packaging copyright notice in debian/copyright.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 15 Jul 2007 16:53:49 +0200

libemail-localdelivery-perl (0.215-1) unstable; urgency=low

  * New upstream release
  * debian/control: added me to uploaders

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sun, 22 Apr 2007 19:22:51 +0200

libemail-localdelivery-perl (0.214-1) unstable; urgency=low

  * New upstream release.
  * Change {build,} dependency on libemail-simple-perl to >= 1.998.
  * Adapt patch 10mbox to new upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 15 Apr 2007 15:46:02 +0200

libemail-localdelivery-perl (0.21-1) unstable; urgency=low

  * New upstream release.
  * Upgrade to Standards-Version 3.7.2. No changes needed.
  * Update URL to the upstream homepage in the long description.
  * Add libtest-pod-perl and libtest-pod-coverage-perl to Build-Depends-Indep.

 -- Niko Tyni <ntyni@iki.fi>  Thu, 27 Jul 2006 22:00:39 +0300

libemail-localdelivery-perl (0.09-1) unstable; urgency=low

  * Initial Release. (Closes: #194548)
  * debian/patches:
    + 10mbox-locking.dpatch: Policy-compliant mbox locking (hopefully)

 -- Niko Tyni <ntyni@iki.fi>  Tue, 20 Dec 2005 19:13:01 +0200
